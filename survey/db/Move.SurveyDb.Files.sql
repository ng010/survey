﻿--ALTER DATABASE [SurveyDb] SET OFFLINE;

/* move data file */
/*
ALTER DATABASE [SurveyDb] 
MODIFY FILE ( 
	NAME = SurveyDb, 
	FILENAME = 'F:\Projects\git\survey\survey\db\SurveyDb.mdf' 
);  
*/

/* move log file */
/*
ALTER DATABASE [SurveyDb] 
MODIFY FILE ( 
	NAME = SurveyDb_log, 
	FILENAME = 'F:\Projects\git\survey\survey\db\SurveyDb.ldf' 
);  
*/

--ALTER DATABASE [SurveyDb] SET ONLINE;

/* verify */
/**/
SELECT name, physical_name AS CurrentLocation, state_desc  
FROM sys.master_files  
WHERE database_id = DB_ID(N'SurveyDb'); 
/**/