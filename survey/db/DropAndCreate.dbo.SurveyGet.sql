﻿USE [SurveyDb]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
	SELECT [name] 
		FROM sys.objects 
		WHERE OBJECT_ID = OBJECT_ID('dbo.SurveyGet') AND type = 'P')
	DROP PROCEDURE [dbo].[SurveyGet]
GO

-- =============================================
-- Author:		NGolovin
-- Create date: 2019-05-07
-- Description:	The procedure returns all entries or single entry from Survey table
-- To return all entries pass no parameters or @id = 0
-- =============================================
CREATE PROCEDURE [dbo].[SurveyGet] 
	@id INT NULL = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF (ISNULL(@id, 0) = 0)
		SELECT S.Id, G.[Name] AS Gender, PM.[Name] AS PhoneManufacturer
			FROM [dbo].[Survey] AS S
			JOIN [dbo].[Gender] AS G ON G.Id = S.GenderId
			JOIN [dbo].[SurveyPhoneManufacturerBridge] AS SPM ON SPM.SurveyId = S.Id
			JOIN [dbo].[PhoneManufacturer] AS PM ON PM.Id = SPM.PhoneManufacturerId

	ELSE 
		SELECT S.Id, G.[Name], PM.[Name]
			FROM [dbo].[Survey] AS S
			JOIN [dbo].[Gender] AS G ON G.Id = S.GenderId
			JOIN [dbo].[SurveyPhoneManufacturerBridge] AS SPM ON SPM.SurveyId = S.Id
			JOIN [dbo].[PhoneManufacturer] AS PM ON PM.Id = SPM.PhoneManufacturerId
			WHERE S.Id = @id
END
