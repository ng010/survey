﻿USE [SurveyDb]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
	SELECT [name] 
		FROM sys.objects 
		WHERE OBJECT_ID = OBJECT_ID('dbo.GenderGet') AND type = 'P')
	DROP PROCEDURE [dbo].[GenderGet]
GO

-- =============================================
-- Author:		NGolovin
-- Create date: 2019-05-07
-- Description:	The procedure returns all entries Gender table
-- =============================================
CREATE PROCEDURE [dbo].[GenderGet]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [id] AS [key], [Name]
		FROM [dbo].[Gender]
END
