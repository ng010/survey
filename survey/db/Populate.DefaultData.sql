﻿USE SurveyDb
GO

/* empty Gender table */
IF EXISTS(SELECT TOP 1 Id FROM [dbo].[Gender])
	DELETE FROM [dbo].[Gender]

INSERT INTO [dbo].[Gender] 
	([Name]) 
	VALUES 
	 (N'Мужской')
	,(N'Женский')

/* empty PhoneManufacturer table */
IF EXISTS(SELECT TOP 1 Id FROM [dbo].[PhoneManufacturer])
	DELETE FROM [dbo].[PhoneManufacturer]

INSERT INTO [dbo].[PhoneManufacturer] 
	([Name]) 
	VALUES 
	 (N'Apple')
	,(N'Samsung')
	,(N'Xiaomi')
	,(N'Nokia')
	,(N'Другой')