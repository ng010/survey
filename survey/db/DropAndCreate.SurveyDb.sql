﻿DROP DATABASE [SurveyDb]

CREATE DATABASE [SurveyDb]
ON PRIMARY ( 
	NAME=SurveyDb, 
	FILENAME = 'F:\Projects\git\survey\survey\db\SurveyDb.mdf',
	SIZE = 5MB,
    MAXSIZE = 15MB,
    FILEGROWTH = 5%
)
LOG ON ( 
	NAME=SurveyDb_log, 
	FILENAME = 'F:\Projects\git\survey\survey\db\SurveyDb.ldf',
	SIZE = 2MB,
    MAXSIZE = 5MB,
    FILEGROWTH = 5%
)

ALTER DATABASE [SurveyDb] SET RECOVERY SIMPLE;