﻿USE SurveyDb
GO

IF EXISTS (
	SELECT TABLE_NAME 
		FROM INFORMATION_SCHEMA.TABLES 
		WHERE TABLE_NAME = N'SurveyPhoneManufacturerBridge' AND TABLE_SCHEMA = 'dbo')
	DROP TABLE [dbo].[SurveyPhoneManufacturerBridge]
GO

CREATE TABLE [dbo].[SurveyPhoneManufacturerBridge] (
    [SurveyId] INT NOT NULL
	CONSTRAINT FK_Survey_Id FOREIGN KEY (SurveyId)
		REFERENCES [dbo].[Survey] (Id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
    [PhoneManufacturerId] INT NOT NULL
	CONSTRAINT FK_PhoneManufacturer_Id FOREIGN KEY (PhoneManufacturerId)
		REFERENCES [dbo].[PhoneManufacturer] (Id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);