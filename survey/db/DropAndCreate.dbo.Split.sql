﻿USE [SurveyDb]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
	SELECT [name]
		FROM sys.objects 
		WHERE OBJECT_ID = OBJECT_ID('dbo.Split') AND [type] IN (N'FN', N'IF',N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[Split]
GO

CREATE FUNCTION [dbo].[Split] ( @stringToSplit NVARCHAR(MAX), @separator NVARCHAR(50) = ',' )
RETURNS
	@returnList TABLE ([Name] [nvarchar] (500))
AS
BEGIN

	DECLARE @name NVARCHAR(255)
	DECLARE @position INT

	WHILE CHARINDEX(@separator, @stringToSplit) > 0
	BEGIN
		SET @position  = CHARINDEX(@separator, @stringToSplit)  
		SET @name = SUBSTRING(@stringToSplit, 1, @position-1)

		INSERT INTO @returnList
			SELECT @name

		SET @stringToSplit = SUBSTRING(@stringToSplit, @position+1, LEN(@stringToSplit)-@position)
	END

	INSERT INTO @returnList
		SELECT @stringToSplit

	RETURN
END