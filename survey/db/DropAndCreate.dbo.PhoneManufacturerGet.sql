﻿USE [SurveyDb]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
	SELECT [name] 
		FROM sys.objects 
		WHERE OBJECT_ID = OBJECT_ID('dbo.PhoneManufacturerGet') AND type = 'P')
	DROP PROCEDURE [dbo].[PhoneManufacturerGet]
GO

-- =============================================
-- Author:		NGolovin
-- Create date: 2019-05-07
-- Description:	The procedure returns all entries from PhoneManufacturer table
-- =============================================
CREATE PROCEDURE [dbo].[PhoneManufacturerGet]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [id] as [key], [Name]
		FROM [dbo].[PhoneManufacturer]
END
