﻿USE [SurveyDb]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
	SELECT [name] 
		FROM sys.objects 
		WHERE OBJECT_ID = OBJECT_ID('dbo.SurveyCreate') AND [type] = 'P')
	DROP PROCEDURE [dbo].[SurveyCreate]
GO

-- =============================================
-- Author:		NGolovin
-- Create date: 2019-05-07
-- Description:	The procedure adds an entry to the Survey table
-- =============================================
CREATE PROCEDURE [dbo].[SurveyCreate] 
	@Gender nvarchar(500), 
	@PhoneManufacturer nvarchar(500)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @errorMessage nvarchar(max), @newId INT, @GenderId INT

	IF (ISNULL(@Gender, '') = '')
	    SET @errorMessage = CONCAT(@errorMessage,N'Gender must be specified explicitly. ');

	IF (ISNULL(@PhoneManufacturer, '') = '')
		SET @errorMessage = CONCAT(@errorMessage,N'Phone Manufacturer must be specified explicitly. ');

	IF NOT EXISTS (
		SELECT id 
			FROM [dbo].[Gender] WITH (NOLOCK) 
			WHERE [Name] = @Gender)
		SET @errorMessage = CONCAT(@errorMessage,N'Specified Gender is not found. ');

	IF EXISTS (
		SELECT PM.Id as PhoneManufacturerId
			FROM [dbo].[PhoneManufacturer] AS PM WITH (NOLOCK) 
			JOIN [dbo].[Split](@PhoneManufacturer, default) AS S ON S.[Name] = PM.[Name]
			WHERE id IS NULL)
		SET @errorMessage = CONCAT(@errorMessage,N'One of the specified Phone Manufacturers is not found. ');

	IF (LEN(@errorMessage) > 0)
		BEGIN
			SET @errorMessage = CONCAT(N'SQL_ERROR: ',@errorMessage,'Survey is not created. ');
			RAISERROR (@errorMessage, 16, 1);
		END

	ELSE 
		BEGIN
			BEGIN TRY 
				BEGIN TRAN CreateSurvey;

					SET @errorMessage = CONCAT(@errorMessage, 'Searching the GenderId from [Gender] by Name. ')
					SELECT @GenderId = Id 
						FROM [dbo].[Gender] 
						WHERE [Name] = @Gender

					SET @errorMessage = CONCAT(@errorMessage, 'Inserting new entry into [Survey]. ')
					INSERT INTO [Survey]
						([GenderId])
						VALUES
						(@GenderId)

					SET @errorMessage = CONCAT(@errorMessage, 'Getting the new Survey Id. ')
					SELECT @newId = CAST(SCOPE_IDENTITY() as INT)

					SET @errorMessage = CONCAT(@errorMessage, 'Inserting [Survey]-[PhoneManufacturer] correspondence into junction table SurveyPhoneManufacturerBridge. ')
					INSERT INTO [dbo].[SurveyPhoneManufacturerBridge] 
						SELECT 
							SurveyId = @newId, 
							PhoneManufacturerId
							FROM (
								SELECT PM.Id as PhoneManufacturerId
									FROM [dbo].[PhoneManufacturer] AS PM
									JOIN [dbo].[Split](@PhoneManufacturer, default) AS S ON S.[Name] = PM.[Name]
							) AS P
				COMMIT TRAN CreateSurvey;
			END TRY
			BEGIN CATCH
				IF(@@TRANCOUNT > 0)
					ROLLBACK TRAN CreateSurvey;
				SET @errorMessage = CONCAT(
					N'SQL_ERROR: Server is not created. Transaction rolled back. ', 
					ERROR_MESSAGE(),
					N'Stack: ',
					@errorMessage
				)

				RAISERROR (@errorMessage, 16, 1);
			END CATCH
		END
END
