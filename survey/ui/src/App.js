﻿import React, { Component } from 'react';
import SurveyForm from './components/SurveyForm'

export default class App extends Component {
  render() {
    return (
      <div id="App" className="col-md-6">
        <h1>Survey Form</h1>
        <SurveyForm />
      </div>
    );
  }
}
