﻿import React, { Component } from 'react';
import 'whatwg-fetch';

export default class SurveyForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            genderOptions: [],
            phoneManufacturerOptions: [],
            newSurvey: {
                gender: '',
                phoneManufacturers: []
            },
            formIsValid: false,
            surveys: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClearForm = this.handleClearForm.bind(this);
        this.handleCheckboxGroupChange = this.handleCheckboxGroupChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    getNamePropertyOfObjectWithMinKey(arrayOfObjects) {
        let name = '';
        if (arrayOfObjects.length > 0) { 
            const minKey = Math.min(...arrayOfObjects.map(obj => obj.key));
            name = arrayOfObjects.filter(obj => obj.key === minKey)[0].name;
        }
        return name;
    }

    componentDidMount() {
        Promise.all([
            fetch('/api/Gender/').then(value => value.json()),
            fetch('/api/PhoneManufacturer/').then(value => value.json()),
            fetch('/api/Survey/').then(value => value.json())
        ]).then(responses => {
            const genderOptions = responses[0];
            const phoneManufacturerOptions = responses[1];
            const surveys = responses[2];

            this.setState(() => {
                return {
                    genderOptions: genderOptions,
                    phoneManufacturerOptions: phoneManufacturerOptions,
                    newSurvey: {
                        gender: this.getNamePropertyOfObjectWithMinKey(genderOptions),
                        phoneManufacturers: []
                    },
                    surveys: surveys
                }
            }, () => {
                console.log('state after fetching data: ');
                console.log(this.state);
            });

        }).catch(error => {
            console.log(error);
        });
    }

    updateTable() {
        fetch('/api/Survey/').then(value => value.json()).then(response => {
            this.setState(() => {
                return {
                    surveys: response
                }
            })
        });
    }

    handleChange(e) {
        const value = e.target.value;
        const name = e.target.name;

        this.setState(
            prevState => ({
                newSurvey: {
                    ...prevState.newSurvey,
                    [name]: value
                }
            }),
            () => console.log(this.state.newSurvey)
        );
    }

    handleCheckboxGroupChange(e) {
        const value = e.target.value;
        let newphoneManufacturers = [...this.state.newSurvey.phoneManufacturers];
        const index = newphoneManufacturers.indexOf(value);

        if (index === -1) {
            newphoneManufacturers.push(value);
        } else {
            newphoneManufacturers.splice(index, 1);
        }

        this.setState(
            prevState => ({
                newSurvey: {
                    ...prevState.newSurvey,
                    phoneManufacturers: newphoneManufacturers
                },
                formIsValid: newphoneManufacturers.length > 0
            }),
            () => console.log(this.state.newSurvey)
        );
    }

    handleSubmit(e) {
        e.preventDefault();
        const parameters = {
            Gender: this.state.newSurvey.gender,
            PhoneManufacturer: this.state.newSurvey.phoneManufacturers.join(",")
        };

        fetch('/api/Survey/', {
            method: "POST",
            body: JSON.stringify(parameters),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            response.json().then(data => {
                console.log("Successful AJAX: " + data);
            });
        }).then(() => {
            this.handleClearForm()
        }).then(() => {
            this.updateTable();
        });

        console.log('form submitted!');
    }

    handleClearForm(e) {
        //e.preventDefault();
        this.setState({
            newSurvey: {
                gender: this.getNamePropertyOfObjectWithMinKey(this.state.genderOptions),
                phoneManufacturers: [],
            },
            formIsValid: false
        });
        console.log('form cleared!');
    }

    render() {
        return (
            <div>
                <form className="container-fuild" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="Gender" className="form-label">Gender:</label>
                        <div>
                            {this.state.genderOptions.map(gender => {return (
                                <label key={gender.key} className="btn btn-secondary">
                                    <input
                                        id={gender.key}
                                        name={"gender"}
                                        value={gender.name}
                                        onChange={this.handleChange}
                                        checked={this.state.newSurvey.gender === gender.name}
                                        type="radio"
                                    />
                                    {gender.name}
                                </label>
                            )})}
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="form-label">Phone Manufacturers:</label>
                        <div>
                            {this.state.phoneManufacturerOptions.map(phoneOption => {return (
                                <label key={phoneOption.key} className="btn btn-secondary">
                                    <input
                                        id={phoneOption.key}
                                        name={"phoneManufacturers"}
                                        value={phoneOption.name}
                                        onChange={this.handleCheckboxGroupChange}
                                        checked={this.state.newSurvey.phoneManufacturers.indexOf(phoneOption.name) > -1}
                                        type="checkbox"
                                    />
                                    {phoneOption.name}
                                </label>
                            )})}
                        </div>
                    </div>
                    <button type="button"
                        className="btn btn-primary"
                        onClick={this.handleSubmit}
                        disabled={!this.state.formIsValid}
                    >Save</button>{" "}
                    <button type="button"
                        className="btn btn-secondary"
                        onClick={this.handleClearForm}
                    >Clear</button>
                </form>
                <div>
                    <h3>Current surveys:</h3>
                    <div className="container">
                        {this.state.surveys.map(survey => {return (
                            <div key={ survey.id + survey.gender + survey.phoneManufacturer } className="row">
                                <div className="col-sm">
                                    {survey.id} - {survey.gender} - {survey.phoneManufacturer}
                                </div>
                            </div>
                        )})}
                    </div>
                </div>
            </div>
        );
    }
}

