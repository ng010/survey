﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using survey.infrastructure;

namespace survey.controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class GenderController: Controller {
        private string connectionString { get; set; }

        public GenderController(IOptions<AppOptions> optionsAccessor) {
            connectionString = optionsAccessor.Value.ConnectionString;
        }

        // GET: api/Gender
        [HttpGet]
        public JsonResult Get() {
            var sqlServerResponse = new SqlServerProxy(connectionString)
                .RunStoredProcedure("GenderGet", new Dictionary<string, object>());
            return Json(sqlServerResponse.Parse());
        }
    }
}