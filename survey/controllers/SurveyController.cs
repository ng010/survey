﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using survey.infrastructure;
using survey.models;

namespace survey.controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class SurveyController:Controller {
        private string connectionString { get; set; }

        public SurveyController(IOptions<AppOptions> optionsAccessor) {
            connectionString = optionsAccessor.Value.ConnectionString;
        }

        // GET: api/Survey
        [HttpGet]
        public JsonResult Get() {
            var sqlServerResponse = new SqlServerProxy(connectionString)
                .RunStoredProcedure("SurveyGet", new Dictionary<string, object>());
            return Json(sqlServerResponse.Parse());
        }

        // GET: api/Survey/5
        [HttpGet("{id}", Name = "Get")]
        public JsonResult Get(int id) {
            var parameters = new Dictionary<string, object>();
            parameters.Add("id", id);
            var sqlServerResponse = new SqlServerProxy(connectionString)
                .RunStoredProcedure("SurveyGet", parameters);
            return Json(sqlServerResponse.Parse());
        }

        // POST: api/Survey
        [HttpPost]
        //[Route("Create")]
        public IActionResult Post([FromBody] Survey survey) {
            var parameters = new Dictionary<string, object>();
            parameters.Add("Gender", survey.Gender);
            parameters.Add("PhoneManufacturer", survey.PhoneManufacturer);
            var sqlServerResponse = new SqlServerProxy(connectionString)
                .RunStoredProcedure("SurveyCreate", parameters);
            return Json(sqlServerResponse.Parse());
        }
    }
}
