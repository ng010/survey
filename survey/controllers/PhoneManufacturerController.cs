﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using survey.infrastructure;

namespace survey.controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneManufacturerController: Controller {
        private string connectionString { get; set; }

        public PhoneManufacturerController(IOptions<AppOptions> optionsAccessor) {
            connectionString = optionsAccessor.Value.ConnectionString;
        }

        // GET: api/Gender
        [HttpGet]
        public JsonResult Get() {
            var sqlServerResponse = new SqlServerProxy(connectionString)
                .RunStoredProcedure("PhoneManufacturerGet", new Dictionary<string, object>());
            return Json(sqlServerResponse.Parse());
        }
    }
}