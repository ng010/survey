﻿using System.Data;

namespace survey.infrastructure {
    public class SqlServerResponse {
        public DataTable Data { get; set; }
        public string Error { get; set; }

        public object Parse() {
            object result;
            if (string.IsNullOrEmpty(Error)) {
                result = Data;
            } else {
                result = Error;
            }
            return result;
        }
    }
}
