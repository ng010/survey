﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace survey.infrastructure {
    public class SqlServerProxy {
        private string _connectionString;
        private int _sqlTimeoutSeconds;
        private SqlServerResponse _response;

        public SqlServerProxy(string connectionString, int sqlTimeoutSeconds = 60) {
            _connectionString = connectionString;
            _sqlTimeoutSeconds = sqlTimeoutSeconds;
            _response = new SqlServerResponse();
        }

        public SqlServerResponse RunStoredProcedure(string storedProcedureName, Dictionary<string, object> parameters, string resultName = "row") {
            DataTable procedureData = new DataTable(resultName);

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString)) {
                try {
                    sqlConnection.Open();

                    using (SqlCommand sqlCommand = new SqlCommand(storedProcedureName, sqlConnection)) {
                        sqlCommand.CommandTimeout = _sqlTimeoutSeconds;
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        // Add parameters.
                        foreach (KeyValuePair<string, object> pair in parameters) {
                            sqlCommand.Parameters.Add(new SqlParameter(pair.Key, RemoveQuotas(pair.Value)));
                        }

                        using (SqlDataReader reader = sqlCommand.ExecuteReader()) {
                            procedureData.Load(reader);
                        }
                    }

                    _response.Data = procedureData;

                } catch (Exception exception) {
                    _response.Error = exception.Message;

                } finally {
                    if (sqlConnection.State.Equals(ConnectionState.Open)) {
                        sqlConnection.Close();
                    }
                }
            }

            return _response;
        }
        private static object RemoveQuotas(object str) {
            string value = str as string;

            if (value != null) {
                if ((value.StartsWith("'")) && (value.EndsWith("'")) && (value.Length > 1)) {
                    return value.Substring(1, value.Length - 2);
                }
            }

            return str;
        }
    }
}
