﻿namespace survey.models {
    public class Survey {
        public string Gender { get; set; }
        public string PhoneManufacturer { get; set; }
        
        public Survey(string Gender, string PhoneManufacturer) {
            this.Gender = Gender;
            this.PhoneManufacturer = PhoneManufacturer;
        }
    }
}
